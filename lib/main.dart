import 'dart:async';
import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

const _duration = Duration(milliseconds: 1500);

double randomBorderRadius() {
  return Random().nextDouble() * 64;
}

double randomMargin() {
  double value = Random().nextDouble() * 64;
  while (value > 45 || value < 10) {
    value = Random().nextDouble() * 64;
  }
  return value;
}

Color randomColor() {
  return Color.fromRGBO(
      Random().nextInt(256), Random().nextInt(256), Random().nextInt(256), 1);
}

class MyAnimationWidget extends StatefulWidget {
  _MyAnimationWidgetState createState() => _MyAnimationWidgetState();
}

class _MyAnimationWidgetState extends State<MyAnimationWidget> {
  Color color;
  double borderRadius;
  double margin;
  int _width;
  int _height;
  Timer timer;

  @override
  void initState() {
    super.initState();
    _height = Random().nextInt(200) + 100;
    _width = Random().nextInt(200) + 100;
    color = Colors.deepPurple;
    borderRadius = randomBorderRadius();
    margin = randomMargin();
    timer = Timer.periodic(Duration(seconds: 2), (Timer t) => triggerAnimation());
  }

  void triggerAnimation() {
    setState(() {
      color = randomColor();
      borderRadius = randomBorderRadius();
      margin = randomMargin();
      _height = Random().nextInt(200) + 100;
      _width = Random().nextInt(200) + 100;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedContainer(
        duration: _duration,
        width: _width.toDouble(),
        height: _height.toDouble(),
        curve: Curves.elasticOut,
        child: AnimatedContainer(
          margin: EdgeInsets.all(margin),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          duration: _duration,
          curve: Curves.elasticOut,
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: MyAnimationWidget()
      ),
    );
  }
}
